<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function show($id){
    	$article = Article::findOrFail($id);
    	return View('article', ['article'=>$article]);
    	}//

    public function list(){
    	$articles = Article::all()->sortByDesc('publish_date');
    	return View('articles', ['articles'=>$articles]);
    	}
}
