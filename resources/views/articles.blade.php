<!DOCTYPE html>
<html>
<head>
    <title></title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>

    <h1>L'actu du numérique</h1>

        <nav>
            <div><a href="{{ url('login')}}">hhhh</div>
        </nav>


    <div class="container">
        <div class="card-columns">
            @foreach ($articles as $article)
                <div class="card">
                    <h2 class="card-title"><a href="{{route('article',['id'=>$article->id])}}">{{$article->title}}</a><h2>
                        <img src="{{$article->image}}" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">{{$article->publish_date}}</p>
                        <p class="card-text">by {{$article->by->username}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    
    <button id="totop">Retour</button>

<script type="text/javascript"></script>

</body>
</html>