@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                </div>
            </div>
        </div>
    </div>
</div>

 <div class="container">
        <div class="card-columns">
            @foreach ($articles as $articles)
                <div class="card">
                    <h2 class="card-title">{{$articles->title}}<h2>
                        <img src="{{$articles->image}}" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">{{$articles->publish_date}}</p>
                        <p class="card-text">by {{$articles->by->username}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection



