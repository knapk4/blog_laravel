<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>



    <form action="/connexion" method="post" class="section">
     {{ csrf_field() }}

        <div class="field">
            <label class="label">User</label>
            <div class="control">
                <input class="input" type="username" name="username" value="{{ old('username') }}">
            </div>
          
                <p class="help is-danger"></p>
            
        </div>

        <div class="field">
            <label class="label">Mot de passe</label>
            <div class="control">
                <input class="input" type="password" name="password">
            </div>
          
                <p class="help is-danger"></p>
          
        </div>

        <div class="field">
            <div class="control">
                <button class="button is-link" type="submit">Se connecter</button>
            </div>
        </div>
    </form>

</body>

</html>